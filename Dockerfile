FROM            registry.gitlab.com/rustydb/docker/centos:8
MAINTAINER      Russell Bunch <rusty@4lambda.io>

# Install DNF and RPMFusion Repos.
RUN             yum -y install \
                    yum-utils \
                    https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm \
                    https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm \
                && yum-config-manager --enable PowerTools

# Install Unifi.
RUN             yum -y install \
                    unifi \
                    https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.2/x86_64/RPMS/mongodb-org-server-4.2.1-1.el7.x86_64.rpm \
                && yum -q clean all

# Open our inform, ap/ui, http redirect, https redirect, and stun ports and set our desired version.
EXPOSE          8080/tcp 8443/tcp 8880/tcp 8843/tcp 3478/udp

# Done; expose and run the app but allow circumvention of launch for poking around.
WORKDIR         /var/lib/unifi
RUN             mkdir -p /var/lib/unifi/data /var/log/unifi && chown unifi:unifi /var/lib/unifi/data /var/log/unifi
VOLUME          ["/var/lib/unifi/data", "/var/log/unifi"]
USER            unifi
CMD             ["java", "-jar", "/usr/share/unifi/lib/ace.jar", "start"]

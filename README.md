# Unifi Controller for CentOS

This Unifi Controller Docker image is based on CentOS 7.

This container exposes two volumes:

```bash
/opt/UniFi/data - UniFi configuration data and DBs
/opt/UniFi/logs - UniFi and MongoDB logs for troubleshooting
```

Additionally, you can set `UNIFI_VERSION` for your UniFi installation. Find CentOS UniFi Versions [here](https://community.ubnt.com/t5/UniFi-Wireless/Unofficial-RHEL-CentOS-UniFi-Controller-rpm-packages/td-p/1744595).
```bash
UNIFI_VERSION     # Version of UniFi to install
```

### License & Copyright

MIT
Copyright 2018 4Lambda LLC; Russell Bunch
